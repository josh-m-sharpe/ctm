## Setup:

Copy .env into place and update values:

`cp .env.sample .env`

## Install Gems:

`bundle`

## Setup DB:

`bundle exec rake db:setup db:migrate`

## Start the app:

`bundle exec foreman start`

## Open the app:

The app should be running on `http://localhost:5000`

