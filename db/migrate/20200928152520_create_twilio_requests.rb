class CreateTwilioRequests < ActiveRecord::Migration[6.0]
  def change
    create_table :twilio_requests do |t|

      t.json :raw_params, null: false
      t.datetime :processed_at

      t.timestamps
    end
  end
end
