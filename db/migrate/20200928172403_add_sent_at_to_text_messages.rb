class AddSentAtToTextMessages < ActiveRecord::Migration[6.0]
  def change
    add_column :text_messages, :sent_at, :datetime
  end
end
