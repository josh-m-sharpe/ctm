class CreateTextMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :text_messages do |t|

      t.references :phone, null: false, index: true
      t.string :message, null: false
      t.boolean :inbound, null: false

      t.timestamps
    end
  end
end

