Rails.application.routes.draw do
  resources :phones do
    resources :messages, only: [:index, :create]
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resource :twilio, only: :create, controller: 'twilio'

  mount Resque::Server.new, :at => "/resque"

  root to: redirect('/phones')
end

