class SendSmsToTwilioJob < ApplicationJob
  queue_as :default

  def perform(text_message_id)
    TextMessage.transaction do
      text_message = TextMessage.lock.find(text_message_id)

      text_message.send!
    end
  end
end
