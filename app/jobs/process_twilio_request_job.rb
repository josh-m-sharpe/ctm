class ProcessTwilioRequestJob < ApplicationJob
  queue_as :default

  def perform(twilio_request_id)
    TwilioRequest.transaction do
      twilio_request = TwilioRequest.lock.find(twilio_request_id)

      twilio_request.process!
    end
  end
end
