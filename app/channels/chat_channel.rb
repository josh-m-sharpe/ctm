class ChatChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "phone_#{params[:id]}"
    stream_from "phone"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
