import $ from "jquery";

let path;

export const Messages = {

  init: function(p) {
    path = p

    this.retrieve_messages();
    this.handle_message_submit();
  },

  retrieve_messages: function() {
    $.get(path, function(resp){
      $('#messages').html(resp.body);
    });
  },

  handle_message_submit: function() {
    $('#new_text_message').on('ajax:success', function(a,b,c){

      $('#text_message_message').val('');
      Messages.retrieve_messages();
    });
  }
}

