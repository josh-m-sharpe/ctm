import consumer from "./consumer"
import { Messages } from '../messages';

consumer.subscriptions.create({channel: "ChatChannel"}, {
  connected() {
    // console.log('connected');
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    // console.log(data);
    Messages.retrieve_messages();
  }
});

