class TwilioController < ApplicationController

  skip_before_action :verify_authenticity_token
  before_action :verify_api_token

  def create
    TwilioRequest.create!(raw_params: params)

    head 200
  end

  def verify_api_token
    unless params[:twilio_api_token] == ENV['TWILIO_API_TOKEN']
      head 401
    end
  end
end

