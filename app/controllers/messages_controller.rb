class MessagesController < ApplicationController

  def index
    phone = Phone.find(params[:phone_id])
    @text_messages = phone.text_messages.order(created_at: :desc).limit(10).reverse

    render json: {
      body: render_to_string(partial: 'index')
    }
  end

  def create
    @phone = Phone.find(params[:phone_id])
    @phone.create_message!(message: params[:text_message][:message])

    head 200
  end
end

