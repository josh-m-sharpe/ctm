class TwilioRequest < ApplicationRecord

  after_commit :queue_process_job!, on: :create

  def process!
    if processed_at.blank?
      phone = Phone.find_by(number: raw_params['From'][-10 .. -1])

      if phone.present?
        TextMessage.create!(
          phone: phone,
          inbound: true,
          message: raw_params['Body']
        )

        self.update!(processed_at: Time.now)

        ActionCable.server.broadcast 'phone', update: true
      end
    end

    true
  end

  def queue_process_job!
    ProcessTwilioRequestJob.perform_later(id)
  end
end

