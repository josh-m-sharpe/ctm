class TextMessage < ApplicationRecord

  after_commit :queue_send_to_twilio, on: :create
  belongs_to :phone

  def send!
    if sent_at.blank?
      client = Twilio::REST::Client.new(
        ENV['TWILIO_ACCOUNT_SID'],
        ENV['TWILIO_AUTH_TOKEN']
      )

      response = client.messages.create(
        from: ENV['TWILIO_PHONE_NUMBER'],
        to: "+1#{phone.number}",
        body: message
      )

      if response.error_code.blank?
        update!(sent_at: Time.now)
      end
    end

    true
  end

private

  def queue_send_to_twilio
    if !inbound?
      SendSmsToTwilioJob.perform_later(id)
    end
  end
end

