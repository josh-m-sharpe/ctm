class Phone < ApplicationRecord

  has_many :text_messages

  validate :number_is_valid

  before_save :strip_number

  def create_message!(message:)
    self.text_messages.create!(
      message: message,
      inbound: false
    )
  end

private

  def number_is_valid
    unless valid_number?
      self.errors.add(:number, 'should have exactly 10 digits');
    end
  end

  def valid_number?
    self.number.present? &&
    self.number.match(/\A[\(\)\-\s0-9]+\Z/) &&
    self.number.gsub(/[^0-9]/, '').length == 10
  end

  def strip_number
    if self.number.present?
      self.number = self.number.gsub(/[^0-9]/, '')
    end
  end
end

