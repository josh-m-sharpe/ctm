class PhoneDecorator < Draper::Decorator

  NUMBER_PATTERN = "(%s) %s-%s"

  decorates :phone
  decorates_association :text_messages

  delegate_all

  def number
    if object.number.present?
      parts = [
        object.number[0,3],
        object.number[3,3],
        object.number[6,4],
      ]

      NUMBER_PATTERN % parts
    else
      object.number_was
    end
  end
end
